# Hugo Docker Image

[![](https://images.microbadger.com/badges/image/enra64/hugo-generator.svg)](https://microbadger.com/images/enra64/hugo-generator "Get your own image badge on microbadger.com")
[![](https://images.microbadger.com/badges/version/enra64/hugo-generator.svg)](https://microbadger.com/images/enra64/hugo-generator "Get your own version badge on microbadger.com")

A Docker container changed from @fixl's/docker-hugo to support generating inside of gitlab's CI instead of running the server directly. 

It also includes `imagemagick` and `lftp` to be able to deploy the generated files via FTP and create gallery thumbnails.

